package com.desktop.web.service.guacamole;

import java.util.Arrays;

import javax.websocket.server.ServerEndpointConfig;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.desktop.web.service.auditvideo.AuditVideoService;
import com.desktop.web.service.file.FileService;
import com.desktop.web.service.remotecpe.NatService;
import com.desktop.web.service.remotecpe.RemotecpeService;
import com.desktop.web.service.remotecpe.VirtulNatService;

/**
 * git https://github.com/fatedier/frp
 * 
 *
 * @author baibai
 */
@Service
public class GuacamoleService implements InitializingBean {

    @Value("${frp.server.address}")
    private String GUAC_HOSTNAME;

    @Value("${guac.guac.port}")
    private Integer GUAC_PORT;

    @Autowired
    private LoonServerEndpointExporter serverEndpointExporter;

    @Autowired
    private NatService natService;

    @Autowired
    private VirtulNatService virtulNatService;

    @Autowired
    private FileService fileService;

    @Autowired
    private AuditVideoService auditVideoService;

    @Autowired
    private RemotecpeService remotecpeService;

    @Override
    public void afterPropertiesSet() throws Exception {
        ServerEndpointConfig config = ServerEndpointConfig.Builder.create(LoonGuacamoleWebSocketTunnelEndpoint.class, "/websocket-tunnel").subprotocols(Arrays.asList(new String[]{"guacamole"})).build();
        serverEndpointExporter.getServerContainer().addEndpoint(config);
        LoonGuacamoleWebSocketTunnelEndpoint.GUAC_HOSTNAME = GUAC_HOSTNAME;
        LoonGuacamoleWebSocketTunnelEndpoint.GUAC_PORT = GUAC_PORT;
        LoonGuacamoleWebSocketTunnelEndpoint.natService = natService;
        LoonGuacamoleWebSocketTunnelEndpoint.virtulNatService = virtulNatService;
        LoonGuacamoleWebSocketTunnelEndpoint.fileService = fileService;
        LoonGuacamoleWebSocketTunnelEndpoint.auditVideoService = auditVideoService;
        LoonGuacamoleWebSocketTunnelEndpoint.remotecpeService = remotecpeService;
    }

}
