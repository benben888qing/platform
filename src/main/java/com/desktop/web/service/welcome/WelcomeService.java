/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform . All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of .
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with .
 * 
 * Modified history:
 *   baibai  2021年4月18日 下午5:25:45  created
 */
package com.desktop.web.service.welcome;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.desktop.web.core.comenum.Status;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.Util;
import com.desktop.web.service.config.ConfigService;
import com.desktop.web.service.node.NodeService;
import com.desktop.web.service.user.UserService;
import com.desktop.web.uda.entity.User;

/**
 * 
 *
 * @author baibai
 */
@Service
public class WelcomeService implements InitializingBean {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ConfigService configService;

    @Autowired
    private UserService userService;

    @Autowired
    private NodeService nodeService;

    @Override
    public void afterPropertiesSet() throws Exception {
        initSystem();
    }

    private void initSystem() {
        String uuid = configService.getValue("system_uuid");
        if (!StringUtils.isEmpty(uuid)) {
            return;
        }

        String tempuuid = Util.UUID();
        configService.addValue("system_uuid", tempuuid);
        configService.addValue("system_init_state", Status.NO.toString());

        logger.info("base init system success,uuid:{}", tempuuid);
    }

    /**
     * 获取系统状态
     * 
     * @return
     */
    public Status getSystemState() {
        String value = configService.getValue("system_init_state");
        return Status.valueOf(value);
    }

    /**
     * 初始化系统
     * 
     * @param params
     */
    public void systemInit(Map<String, String> params) {
        String teamName = params.get("teamName");
        String username = params.get("username");
        String password = params.get("password");

        if (StringUtils.isEmpty(teamName) || StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            throw new BusinessException("参数不完整，请核对");
        }

        Status state = this.getSystemState();
        if (state == Status.YES) {
            throw new BusinessException("系统已初始化");
        }

        configService.setValue("system_init_state", Status.YES.toString());
        configService.addValue("team_name", teamName);

        User user = new User();
        user.setSelfname("超级管理员");
        user.setUsername(username);
        user.setPassword(password);
        userService.addUser(user);
        userService.addUserRole(UserService.ADMIN_ROLE_ID, user);
        configService.addValue("admin_root_uid", user.getId().toString());
        configService.addValue("init_time", new Date().getTime());

        nodeService.initNode(teamName);

        logger.info("init system success,teamName:{},username:{}", teamName, username);
    }

}
